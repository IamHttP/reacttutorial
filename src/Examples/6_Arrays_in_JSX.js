// ./src/Examples/List.js
import React from "react";
import ReactDOM from "react-dom";

function ArraysInJSX() {
  return (
    <div>
      <h1>Arrays in JSX</h1>
      <ul>
        {[1, 2, 3, 4, 6].map((item, i) => {
          return (
            <li key={i}>
              {item}
            </li>
          )
        })}
      </ul>
    </div>
  )
}

ReactDOM.render(<ArraysInJSX/>, document.getElementById('arraysJSX'));

export default ArraysInJSX;