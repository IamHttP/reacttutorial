// ./src/Examples/Controlled.js
import React from "react";
import ReactDOM from "react-dom";

// Data is handled by the DOM.
// we ask the DOM what the value is.
class UnControlled extends React.Component {
  constructor(props) {
    super(props);
    this.el = React.createRef();
  }
  render() {
    return (
      <div>
        <h3>UnControlled Component</h3>
        <input
          ref={this.el}
          onChange={(e) => {
            if (this.el.current.value.length > 10) {
              this.el.current.value = ''
            }
          }}
        />
      </div>
    )
  }
}

class Controlled extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value:''
    }
  }
  render() {
    return (
      <div>
        <h3>Controlled Component</h3>
        <input
          value={this.state.value}
          onChange={(e) => {
            this.setState({
              value: e.target.value
            });
            if (this.state.value.length > 10) {
              this.setState({value: ''})
            }
          }}
        />
      </div>
    )
  }
}


ReactDOM.render(<div>
  <h1>Form Controls</h1>
  <UnControlled></UnControlled>
  <Controlled></Controlled>
  
</div>, document.getElementById('form_controls'));
export default UnControlled;



