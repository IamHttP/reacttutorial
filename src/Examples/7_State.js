// ./src/Examples/HelloWorld.js
import React from "react";
import ReactDOM from "react-dom";

class State extends React.Component {
  render() {
    return (
      <div>
        <h1>Stateful Components</h1>
        <button
          onClick={() => {
            this.setState({
              count: this.state && this.state.count + 1 || 1
            });
          }}
        >Click to Add One {this.state && this.state.count || 0}</button>
      </div>
    )
  }
}

ReactDOM.render(<State/>, document.getElementById('state'));

export default State;