import React from 'react';
import ReactDOM from "react-dom";

function ChildrenExample(props) {
  console.log(props.children);
  return (
    <div>
      <h3>I am rendering my children</h3>
      {props.children}
    </div>
  )
}

// Modify anything below here
ReactDOM.render(
  <div>
    <h1>Props.Children</h1>
    <ChildrenExample>
      <h3>I am a child</h3>
      <div>Still children</div>
      <ChildrenExample>
        <div>Inner children</div>
      </ChildrenExample>
    </ChildrenExample>
  </div>,
  document.getElementById('exampleChildren')
);

export default ChildrenExample;

