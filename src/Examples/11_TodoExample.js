import React from 'react';
import ReactDOM from "react-dom";

class Todo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: '',
      tasks: []
    }
  }
  
  render() {
    return (
      <div>
        <h1>Stateful Components</h1>
        <ul>
          {
            this.state.tasks.map((item, i) => {
              return <li key={i}> {item}</li>
            })
          }
        </ul>
        <form>
          <input
            name='task'
            value={this.state.task}
            onChange={(e) => {
              this.setState({
                task: e.target.value
              });
            }}
          />
          <button
            onClick={(e) => {
              e.preventDefault();
              this.setState({
                tasks: [...this.state.tasks, this.state.task],
                task: ''
              });
            }}
          >Add
          </button>
        </form>
      </div>
    );
  }
}

ReactDOM.render(<Todo/>, document.getElementById('exampleTODO'));
export default Todo;
