// ./src/Examples/HelloWorld.js
import React from "react";
import ReactDOM from "react-dom";

function Functional(props) {
  let JSX_DIV = null;
  
  // either store the entire thing before
  if (props.age > 15) {
    JSX_DIV = <div>Age is over 15</div>;
  }
  
  // or calculate it inline
  return (
    <div>
      <h1>Functional Component</h1>
      {JSX_DIV}
      <span>Hello</span>
      { 1 > 12 ? <div>Looks like</div> : <div>Logic</div> }
      <span>{props.name}</span>
    </div>
  )
};

ReactDOM.render(<Functional/>, document.getElementById('functional'));

export default Functional;