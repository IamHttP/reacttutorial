import React from 'react';
import ReactDOM from "react-dom";


const FunctionalPureComponent = React.memo(function MyComponent(props) {
  console.log('Rendering Memo Component - Only Once');
  return null;
});


class PureComponent extends React.PureComponent {
  render() {
    console.log('Rendering Pure Component - only once');
    return null;
  }
}

class RegularComponent extends React.Component {
  render() {
    console.log('Rendering Regular Component - Even though nothing is different');
    return null;
  }
}

class Stater extends React.Component {
  render() {
    return (
      <>
        <h1>Pure Components vs Regular Components</h1>
        <button
          onClick={() => {
            this.setState({});
          }}
        >
          click to set state
        </button>
        <PureComponent name='foo'/>
        <RegularComponent name='bar'/>
        <FunctionalPureComponent name='bar'/>
      </>
    )
  }
}

ReactDOM.render(
  <Stater></Stater>,
  document.getElementById('pureComponents')
);

export default Stater;

