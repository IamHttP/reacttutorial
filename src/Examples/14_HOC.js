import React from 'react';
import ReactDOM from "react-dom";


/**
 * The UserList Component has no idea, or relation to the API being used.
 * It has access to a single setID method, provided as a prop
 * It would usually have an onClick method anyway, so setID at this point is used as an example
 */
class UserList extends React.Component {
  render() {
    let {apiData} = this.props;
    let users = apiData.users || [];
    
    return (
      <ul>
        <li>Click on a user and watch the console</li>
        {users.map((user, i) => {
          return <li key={i} onClick={() => {
            this.props.setID(user.id);
          }}>{user.username}
          </li>
        })}
      </ul>
    );
  }
}

/**
 *
 * @param WrappedComponent
 * @param url
 * @param mapApiDataToProps
 * @return {NewWrappingComponent}
 *
 *
 * withData returns a High Order Component that's already wired up to an API
 * ComponentDidMount is already set-up and fetch is implemented once.
 */
function withData(WrappedComponent, url, mapApiDataToProps) {
  class NewWrappingComponent extends React.Component {
    componentDidMount() {
      this.fetchData();
    }
    
    fetchData(id = '') {
      let ID = id ? `/${id}` : '';
      
      fetch(`${url}${ID}`, {
        method: 'GET',
        'Content-Type': 'application/json',
        credentials: 'same-origin'
      }).then((res) => {
        return res.json();
      }).then((data) => {
        this.setState({
          apiData: mapApiDataToProps(data)
        })
      });
    }
    
    render() {
      return (
        <WrappedComponent
          apiData={this.state && this.state.apiData || {}}
          setID={(id) => {
            this.fetchData(id);
          }}
          {...this.props}
        ></WrappedComponent>
      )
    }
  }
  
  return NewWrappingComponent;
}


/**
 *
 * @type {NewWrappingComponent}
 *
 * withData is used to create a UserList that's connected to the API
 */
let UserListWithData = withData(
  UserList,
  'https://jsonplaceholder.typicode.com/users',
  (apiData) => {
    return {
      users: apiData
    }
  });

ReactDOM.render(
  <>
    <h1>High Order Components</h1>
    <UserListWithData/>,
  </>,
  document.getElementById('hoc')
);

export default UserListWithData;

