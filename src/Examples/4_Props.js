// ./src/Examples/HelloWorld.js
import React from "react";
import ReactDOM from "react-dom";

function FunctionalProps(props) {
  return (
    <div>
      <h3>Functional Props</h3>
      <div>{props.name}</div>
    </div>
  )
}

class ClassicalProps extends React.Component {
  render() {
    return (
      <div>
        <h3>Classical Props</h3>
        <div>{this.props.name}</div>
      </div>
    )
  }
}

function PropsExamples() {
  return (
    <div>
      <h1>Props</h1>
      <FunctionalProps
        name='my_functional_prop'
      ></FunctionalProps>
      <ClassicalProps
        name='my_classical_prop'
      ></ClassicalProps>
    </div>
  )
};

ReactDOM.render(<PropsExamples/>, document.getElementById('props'));

export default PropsExamples;