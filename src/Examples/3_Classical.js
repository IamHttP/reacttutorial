// ./src/Examples/HelloWorld.js
import React from "react";
import ReactDOM from "react-dom";


class Classical extends React.Component {
  render() {
    return (
      <div>
        <h1>Classical Component</h1>
        <div> - {this.props.name}</div>
      </div>
    )
  }
}

ReactDOM.render(<Classical/>, document.getElementById('classical'));

export default Classical;