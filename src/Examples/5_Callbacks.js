// ./src/Examples/HelloWorld.js
import React from "react";
import ReactDOM from "react-dom";

function Callbacks(props) {
  return (
    <div>
      <h1>Callbacks (props)</h1>
      <button
        onClick={props.letMeKnow}
      >Click me</button>
    </div>
  )
}

ReactDOM.render(<Callbacks
  letMeKnow={() => {
    alert('This alert was passed as a prop!');
  }}
/>, document.getElementById('callbacks'));

export default Callbacks;