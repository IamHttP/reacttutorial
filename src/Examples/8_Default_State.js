// ./src/Examples/HelloWorld.js
import React from "react";
import ReactDOM from "react-dom";

class State extends React.Component {
  constructor(props) {
    super(props);
    // component did not run yet, we can set the State object directly
    this.state = {
      count: 42
    }
  }
  render() {
    return (
      <div>
        <h1>Default States</h1>
        <button
          onClick={() => {
            this.setState({
              count: this.state.count + 1
            });
          }}
        >Click to Add One {this.state.count}</button>
      </div>
    )
  }
}

ReactDOM.render(<State/>, document.getElementById('defaultState'));

export default State;