import React from 'react';
import ReactDOM from "react-dom";

class LifeCycles extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      counter: 0
    };
  }
  
  componentDidMount() {
    console.log('componentDidMount');
    setInterval(() => {
      console.log('LOOP - Inside didMount');
    }, 20000);
  }
  
  componentWillMount() {
    console.log('componentWillMount');
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 2000);
    });
  }
  
  componentDidUpdate() {
    console.log('componentDidUpdate');
  }
  
  componentWillUpdate() {
    console.log(`componentWillUpdate`);
  }
  
  shouldComponentUpdate(newProps, newState) {
    console.log(`shouldComponentUpdate`);
    // Default behaviour includes immutability checking of props and state
    return true;
  }
  
  componentWillUnmount() {
    console.log('componentWillUnmount - do we need to do any cleanup?');
  }
  
  componentWillReceiveProps(nextProps, nextContext) {
    console.log('componentWillReceiveProps');
  }
  
  render() {
    return (
      <div>
        <h1>Life Cycles</h1>
        <button
          onClick={() => {
            this.setState({
              counter: this.state.counter + 1
            })
          }}
        >
          {this.state.counter}
        </button>
        <button
          onClick={this.props.onUnmountClick}
        >
          Unmount
        </button>
      </div>
    )
  }
}

class Parent extends React.Component {
  render() {
    return !this.die && <LifeCycles
      onUnmountClick={() => {
        this.die = true;
        // notice nothing will happen, we need to force a re-render by giving new props
        // or new state.
        // without new props or new state, the component will be static.
        // there is no 'force update'
        // more on this later
        this.setState({});
      }}
    />;
  }
}

ReactDOM.render(<Parent
/>, document.getElementById('lifecycles'));

export default Parent;