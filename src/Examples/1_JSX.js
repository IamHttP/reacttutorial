// ./src/Examples/Static.js
import React from "react";
import ReactDOM from "react-dom";

let myName = 'world';

let JSX = (
  <div>
    <h1>JSX - Hello {myName}</h1>
    <ul>
      <li>Easy</li>
      <li>Intuitive</li>
      <li>Fun</li>
    </ul>
  </div>
);

ReactDOM.render(JSX, document.getElementById('jsx'));

export default JSX;