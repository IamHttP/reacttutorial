// ./src/index.js
import './style.scss';
import React from 'react';
import ReactDOM from 'react-dom';

// Example code, please do not modify :)
import './Examples/1_JSX';
import './Examples/2_Functional';
import './Examples/3_Classical';
import './Examples/4_Props';
import './Examples/5_Callbacks';
import './Examples/6_Arrays_in_JSX';
import './Examples/7_State';
import './Examples/8_Default_State';
import './Examples/9_FormControls';
import './Examples/10_LifeCycles';
import './Examples/11_TodoExample';
import './Examples/12_Children';
import './Examples/13_PureComponents';
import './Examples/14_HOC';

// Modify anything below here
ReactDOM.render(
  <div>Hello</div>,
  document.getElementById('app')
);