let conf = require('./webpack.config');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = Object.assign(conf, {
  devServer: {
    contentBase: './dev',
    port: 9999
  },
  mode: 'development',
  devtool: 'inline-source-map'
});